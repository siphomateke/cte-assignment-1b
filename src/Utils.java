import java.util.ArrayList;

public class Utils {
  public static boolean isDelimiter(Character ch) {
    return ch == ' ' || ch == '+' || ch == '-' || ch == '*' ||
        ch == '/' || ch == ',' || ch == ';' || ch == '>' ||
        ch == '<' || ch == '=' || ch == '(' || ch == ')' ||
        ch == '[' || ch == ']' || ch == '{' || ch == '}';
  }

  public static boolean isOperator(Character ch) {
    return ch == '+' || ch == '-' || ch == '*' ||
        ch == '/' || ch == '>' || ch == '<' ||
        ch == '=';
  }

  public static boolean validIdentifier(String str) {
    return !(str.charAt(0) == '0' || str.charAt(0) == '1' || str.charAt(0) == '2' ||
        str.charAt(0) == '3' || str.charAt(0) == '4' || str.charAt(0) == '5' ||
        str.charAt(0) == '6' || str.charAt(0) == '7' || str.charAt(0) == '8' ||
        str.charAt(0) == '9' || isDelimiter(str.charAt(0)) == true);
  }

  public static boolean isKeyword(String str) {
    return str.equals("if") || str.equals("else") ||
        str.equals("while") || str.equals("do") ||
        str.equals("break") ||
        str.equals("continue") || str.equals("int") || str.equals("double") || str.equals("float")
        || str.equals("return") || str.equals("char") || str.equals("case") || str.equals("char")
        || str.equals("sizeof") || str.equals("long") || str.equals("short") || str.equals("typedef")
        || str.equals("switch") || str.equals("unsigned") || str.equals("void") || str.equals("static")
        || str.equals("struct") || str.equals("goto");
  }

  public static ArrayList<Token> parse(String input) {
    ArrayList<Token> tokens = new ArrayList<>();

    int left = 0, right = 0;
    int len = input.length();

    while (right < len && left <= right) {
      char ch = input.charAt(right);

      boolean endOfLine = right == len - 1;
      boolean reachedNextDelimiterAfterWord = isDelimiter(ch) && left != right;
      boolean foundWordToken = reachedNextDelimiterAfterWord || endOfLine;
      if (foundWordToken) {
        TokenType type = null;
        String token = input.substring(left, endOfLine ? len : right);

        if (isKeyword(token)) {
          type = TokenType.Keyword;
        } else if (validIdentifier(token)) {
          type = TokenType.Identifier;
        }

        if (type != null && token != null) {
          tokens.add(new Token(type, token));
        }
      }

      if (isDelimiter(ch)) {
        TokenType type = null;
        String token = Character.toString(ch);

        if (isOperator(ch)) {
          type = TokenType.Operator;
        } else if (ch == '(') {
          type = TokenType.OpenBracket;
        } else if (ch == ')') {
          type = TokenType.CloseBracket;
        } else if (ch == '{') {
          type = TokenType.LeftBrace;
        } else if (ch == '}') {
          type = TokenType.RightBrace;
        } else if (ch != ' ') {
          type = TokenType.Symbol;
        }

        if (type != null && token != null) {
          tokens.add(new Token(type, token));
        }
      }

      if (foundWordToken) {
        right++;
        left = right;
      } else if (isDelimiter(ch)) {
        right++;
        left++;
      } else {
        right++;
      }
    }
    return tokens;
  }

  public static void parseAndLog(String input, int lineNumber) {
    ArrayList<Token> tokens = parse(input);
    for (int i = 0; i < tokens.size(); i++) {
      Token token = tokens.get(i);
      System.out.println(String.format("TOKEN#%d %s \t %s", i + 1, token.value, token.type));
    }
    System.out.println(String.format("\nTOTAL NUMBER OF TOKENS FOR STRING#%d: %d", lineNumber, tokens.size()));
    System.out.println(String.format("END OF STRING#%d", lineNumber));
    System.out.println("===================\n");
  }
}
