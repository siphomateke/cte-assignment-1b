enum TokenType {
  Keyword,
  OpenBracket,
  CloseBracket,
  LeftBrace,
  RightBrace,
  Identifier,
  Symbol,
  Operator
}

public class Token {
  TokenType type;
  String value;

  public Token(TokenType type, String value) {
    this.type = type;
    this.value = value;
  }
}