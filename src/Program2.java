import java.util.*;

public class Program2 {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);

    System.out.println("enter string:");
		String delim = " ";
		String mystr = input.nextLine();
        String diff = "diff", mult = "mult", sum = "sum", den = "den", eq = "=", div = "/", minus = "-";
		
		StringTokenizer str =
			new StringTokenizer(mystr, delim);
		
		int count = str.countTokens();
		System.out.println("number of tokens : " + count + "\n");

		int i = 0;
	
		while (str.hasMoreTokens()){

			i++;
			String token = (String) str.nextElement();
			if (token.equals(diff)||token.equals(mult)||token.equals(sum)||token.equals(den)){delim = "Identifier";}
			if (token.equals(div)||token.equals(minus)){delim = "Operator";}
			if (token.equals(eq)){delim = "Symbol";}
			System.out.println("token #" + i + " : "
						+ token + " ("+ delim +")");
		}
		
	}
}
