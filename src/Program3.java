import java.util.InputMismatchException;
import java.util.Scanner;

public class Program3 {
  public static void main(String[] args) {
    Scanner str = new Scanner(System.in);
    System.out.print("Enter number of lines of program: ");
    int numLines = 0;
    try {
      numLines = str.nextInt();
      if (numLines <= 0) {
        System.out.print("Please enter a number greater than 0: ");
      } else {
        str.nextLine();
      }
    } catch (InputMismatchException e) {
      System.out.print("Please enter a valid number.");
    }
    for (int lineNumber = 0; lineNumber < numLines; lineNumber++) {
      System.out.print(String.format("Enter String#%d: ", lineNumber));
      String input = str.nextLine();
      Utils.parseAndLog(input, lineNumber + 1);
    }

    str.close();
  }
}
