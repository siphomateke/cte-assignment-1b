import java.util.*;
public class LeProgram3 {

	
	public static void main(String args[]) {

		Scanner number = new Scanner(System.in);
		System.out.println("number of inputs: ");
		

		String[] input = new String[number.nextInt()];

		for (int i = 0; i < input.length; i++){
			input[i] = number.nextLine();
			System.out.println("String #" + (i+1));
			
			String delim = " ";
			String mystr = number.nextLine();
			String diff = "diff", mult = "mult", sum = "sum", div = "div", eq = "=", slash = "/", minus = "-", plus = "+", times = "*", num = "int", a = "a", b = "b", scanf = "scanf", printf = "printf", open = "(", close = ")", left = "{", right = "}";
			
			StringTokenizer str =
				new StringTokenizer(mystr, delim);
			
			int count = str.countTokens();
			System.out.println("number of tokens : " + count + "\n");
	
			int x = 0;
		
			while (str.hasMoreTokens()){
	
				x++;
				String token = (String) str.nextElement();
				if (token.equals(diff)||token.equals(mult)||token.equals(sum)||token.equals(div)||token.equals(a)||token.equals(b)) {delim = "Identifier";}
				if (token.equals(slash)||token.equals(minus)||token.equals(plus)||token.equals(times)){delim = "Operator";}
				if (token.equals(eq)){delim = "Symbol";}
				if (token.equals(open)){delim = "OpenBracket";}
				if (token.equals(close)){delim = "CloseBracket";}
				if (token.equals(left)){delim = "LeftBrace";}
				if (token.equals(printf)||token.equals(scanf)||token.equals(num)){delim = "Keyword";}
				System.out.println( "token #" + x + " : "
							+ token + " ("+ delim +")");
	
							
			}
			
		}
		}

		
		}
		
	
